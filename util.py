# coding: utf-8

import pymysql


def write_data(table, columns, list_data):
    conn = pymysql.connect(host='8.140.125.13', user="root", passwd='wangjie11', database='wcystation')
    cur = conn.cursor()
    # list_data = [data.decode() for data in list_data]
    params = ['%s'] * len(columns)
    params = ','.join(params)
    columns = [f'`{c}`' for c in columns]
    columns = ','.join(columns)
    cur.executemany(
        f"replace into {table} ({columns}) values ({params})",
        list_data)
    conn.commit()
    cur.close()
    conn.close()
