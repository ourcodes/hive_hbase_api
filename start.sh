#!/bin/bash

#echo $1
#if [ -z "$1" ]
#then
#  echo '需要填写sql参数'
#  exit 1
#fi
while [ true ]; do
  python3 ./hbase_export_data.py
  hive -e "insert overwrite local directory '/tmp/hive_data' select * from default.coupling"
  python3 ./hive_export_data_coupling.py -f /tmp/hive_data/000000_0
  hive -e "insert overwrite local directory '/tmp/hive_data' select * from default.mrt"
  python3 ./hive_export_data_mrt.py -f /tmp/hive_data/000000_0
  sleep 1m
done
