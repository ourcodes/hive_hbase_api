# coding: utf-8

import csv
import argparse
import util


def get_data(data_file):
    lines = []
    with open(data_file, 'r') as f:
        f_reader = csv.reader(f, delimiter='\001')
        for line in f_reader:
            lines.append(line)
    return lines


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='获取参数')
    parser.add_argument('-f', '--data_file', required=True, help="输入的数据文件")
    args = parser.parse_args()
    data = get_data(args.data_file)
    util.write_data('coupling',
                    ['Time', 'velocity', '2_motor_current', '3_motor_current', '6_motor_current', '7_motor_current'],
                    data)
