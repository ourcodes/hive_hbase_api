# coding: utf-8

import happybase
import util
import argparse

hbase_columns = [b'FO_DATA:', b'F_DATA:', b'S_DATA:', b'T_DATA:']


def get_data_from_hbase(host):
    conn = happybase.Connection(host)
    conn.open()
    table = conn.table('ODS_PHM_ONLINE_TRAIN_DATA')
    # table.put('ODS_PHM_ONLINE_TRAIN_DATA', {'FO_DATA:': '213', 'F_DATA:': '23455', 'S_DATA:': '3423', 'T_DATA:': '2323'})
    data = table.row('ODS_PHM_ONLINE_TRAIN_DATA', columns=hbase_columns)
    conn.close()
    return data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='获取参数')
    parser.add_argument('-t', '--host', required=False, help="host")
    args = parser.parse_args()
    data = get_data_from_hbase('127.0.0.1' if args.host is None else args.host)
    data = [d.decode() for d in data.values()]
    data.insert(0, 'ODS_PHM_ONLINE_TRAIN_DATA')
    util.write_data('ods_phm_online_train_data', ['row_key', 'FO_DATA', 'F_DATA', 'S_DATA', 'T_DATA'], [data])
